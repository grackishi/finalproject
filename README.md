# Final Project - Automated Movement #

![](/screenshot.jpg)

For this project I wanted to use the model we trained from the other project, but mine originally did not 
turn out how I wanted it to. I retrained a RunwayML model to recreate images from gymnastics meets so I could
get computer generated versions of movement.If you press the space bar the boxes reset and the Runway image 
is set to a new, random vector. I wanted couple the machine learning creation of a body in motion with
the movement of "pixels" on screen. I wanted these two to work together to create an image that questions
the accuracy of the movement and images created from machine learning as well as how "realistic" these
images really are. Though both the image and the squares are computer generated I wanted to rigid movement of
the squares to contrast the more "fluid" movement of the machine learning model's images. 