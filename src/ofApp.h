#pragma once

#include "ofMain.h"
#include "ofxRunway.h"

class ofApp : public ofBaseApp, public ofxRunwayListener {

public:
	// Need these to set up OF
	void setup();
	void update();
	void draw();

	// To set up spacebar functionality 
	void keyReleased(int key);

	// Set up runway
	ofxRunway runway;

	// Have something to hold and transport image result from runway 
	ofImage runResult;
	vector<float> vect;

	// Necessary for runway
	void runwayInfoEvent(ofJson& info);
	void runwayErrorEvent(string& message);

	// Holds width and height for rectangle vals
	float width[100];
	float height[100];

	// Holds x and y location of rectangles
	float locX[100];
	float locY[100];


};
