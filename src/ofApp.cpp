#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
	// Set window to be same shape as image from runway!
	ofSetWindowShape(1024, 1024);

	// Setup runway so that it actually works
	runway.setup(this, "http://localhost:8000");
	runway.start();

	// Set a value to zero so you can keep track of which 
	// number you are on in the arrays in the for loop
	int value = 0;

	// For loop to initialize all of the values for drawn rectangles
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			width[value] = 100;
			height[value] = 100;
			locY[value] = j * 100;
			locX[value] = i * 100;
			value += 1;
		}
	}

	// Fill vector with random values to start
	for (int i = 0; i < 512; i++) {
		float f = ofRandom(-0.5, -0.5);
		vect.push_back(f);
	}

}

//--------------------------------------------------------------
void ofApp::update() {
	// Make sure runway is not busy, then give it more stuff
	if (!runway.isBusy()) {
		ofxRunwayData data;
		int frame = ofGetFrameNum();

		// Change all the floats in the vector by 0.01 to see change in image
		for (int i = 0; i < 512; i++) {
			// Change the floats by a random numberr between -.1 and .1 instead of using noise
			// Using draw a dino and it looks like a little animation
			vect[i] += ofRandom(-.1, 0.1);
		}

		// Load vect into data
		data.setFloats("z", vect);

		// Send Data to runway!
		runway.send(data);
	}

	//Get the image from runway
	runway.get("image", runResult);

}

//--------------------------------------------------------------
void ofApp::draw() {

	// Draw runway result!
	if (runResult.isAllocated()) {
		
		// Set the color to white to draw full color original image
		ofSetColor(255, 255, 255);
		runResult.draw(0, 0, 1024, 1024);
		// Set the color to blue to put a blue filter over the image
		ofSetColor(0, 25, 176, 100);
		runResult.draw(0, 0, 1024, 1024);

		// Set the color back to white, lower opacity for rectangles
		ofSetColor(255, 255, 255, 30);
		// Draw each of the rectangles and set values so they move around
		// and simulate movement
		for (int i = 0; i < 100; i++) {
			// Change width and height of rectangles
			width[i] -= ofRandom(-3, 3);
			height[i] += ofRandom(-3, 3);

			// Change X location of rectangles
			locX[i] += ofRandom(-3, 3);

			// Draw the rectangles
			ofDrawRectangle(locX[i], locY[i], width[i], height[i]);
		}
	}	
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {
	// Liked this functionality so stole from example code
	if (key == ' ') { //press spacebar to generate a new  or
		vect.clear(); //clear the vector
		for (int i = 0; i < 512; i++) {
			float n = ofRandom(-3.0, 3.0); //load the vector with new floats
			vect.push_back(n);
		}

		// If the spacebar is pressed you reset the location of the rectangles as well
		int value = 0;
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				width[value] = 100;
				height[value] = 100;
				locY[value] = j * 100;
				locX[value] = i * 100;
				value += 1;
			}
		}
	}
}

void ofApp::runwayInfoEvent(ofJson& info) {
	ofLogNotice("ofApp::runwayInfoEvent") << info.dump(2);
}
// if anything goes wrong
//--------------------------------------------------------------
void ofApp::runwayErrorEvent(string& message) {
	ofLogNotice("ofApp::runwayErrorEvent") << message;
}
